from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from .views import home, data
from selenium import webdriver

class TestIndex(TestCase):
    def test_index_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_index_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)
    
    def test_index_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'main/home.html')

    def test_query_is_exist(self):
        response = Client().get(f"{reverse('main:data')}?q=book")
        self.assertEqual(response.status_code, 200)
